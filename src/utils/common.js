/*
 * @Author: Patrick-Jun
 * @Date: 2021-05-28 14:27:47
 * @Last Modified by:   Patrick-Jun
 * @Last Modified time: 2021-05-28 14:27:47
 */

import { getTimeCookie } from '../services/store.service';

/**
 * 常用交互方法
 */

const checkCookieTime = () => {
  const minute = 30;
  const cookieTime = getTimeCookie();
  if (!cookieTime) {
    return false;
  }
  const nowTime = new Date().getTime();
  return (nowTime - cookieTime) / 1000 / 60 < minute;
};

module.exports = {
  checkCookieTime,
};
