/* eslint-disable no-param-reassign */
/*
 * @Author: Patrick-Jun
 * @Date: 2021-06-21 20:11:24
 * @Last Modified by: Patrick-Jun
 * @Last Modified time: 2021-07-10 14:12:03
 */

/**
 * 常用计算方法实现
 */

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
export const parseTime = (time, cFormat, fillZero = true) => {
  if (arguments.length === 0) {
    return null;
  }
  if (!time) {
    return null;
  }
  let format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
  let date;
  if (typeof time === 'object') {
    date = time;
  } else {
    if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
      time = parseInt(time);
    }
    if (typeof time === 'number' && time.toString().length === 10) {
      time = time * 1000;
    }
    date = new Date(time);
  }
  let formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  };
  return format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key];
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
        return ['日', '一', '二', '三', '四', '五', '六'][value];
    }
    if (fillZero) {
        if (result.length > 0 && value < 10) {
            value = '0' + value;
        }
    }
    return value || 0;
  });
}

/**
 * @description 获取今天2020-00-00
 * @param split 分隔符
 * @returns {string} 2020-00-00
 */
export const getToday = split => getTodayTime(split).substr(0, 10);
