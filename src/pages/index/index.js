Page({
  data: {},

  /**
   * @description 跳转页面
   * @param event 事件
   */
  goPage(event) {
    wx.navigateTo({
      url: `inner/inner?from=${event.currentTarget.dataset.from}`,
    });
  },

});
