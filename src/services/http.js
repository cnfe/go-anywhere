/*
 * @Author: Patrick-Jun
 * @Date: 2021-07-10 14:04:07
 * @Last Modified by: Patrick-Jun
 * @Last Modified time: 2021-07-10 14:07:50
 */

/**
 * get请求
 * @param {string} url 请求地址
 * @param {object} params 请求参数
 * @param {boolean} showloading 是否现在加载中
 */
const get = (url, params, showloading) => {
  // 请求头
  const header = {
    'content-type': 'application/json',
  };
  if (showloading) {
    wx.showLoading({
      title: '加载中...',
      mask: false,
    });
  }
  return new Promise((resolved, rejected) => {
    wx.request({
      url,
      data: params,
      header,
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (res) => {
        resolved(res.data.data);
      },
      fail: (res) => {
        rejected({
          msg: '网络开小差啦',
          code: res.statusCode,
        });
      },
      complete: () => {
        if (showloading) {
          wx.hideLoading();
        }
      },
    });
  });
};

/**
 * post请求
 * @param {string} url 请求地址
 * @param {object} params 请求参数
 * @param {boolean} showloading 是否现在加载中
 */
const post = (url, params, showloading) => {
  // 请求头
  const header = {
    'content-type': 'application/json',
  };
  if (showloading) {
    wx.showLoading({
      title: '加载中...',
      mask: false,
    });
  }
  return new Promise((resolved, rejected) => {
    wx.request({
      url,
      data: params,
      header,
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: (res) => {
        resolved(res.data.data);
      },
      fail: (res) => {
        rejected({
          msg: '网络开小差啦',
          code: res.statusCode,
        });
      },
      complete: () => {
        if (showloading) {
          wx.hideLoading();
        }
      },
    });
  });
};

/**
 * 旧get请求
 * @param {string} url 请求地址
 * @param {object} params 请求参数
 */
const cget = (url, params) => new Promise((resolved, rejected) => {
  if (showloading) {
    wx.showLoading({
      title: '加载中...',
      mask: false,
    });
  }
  wx.request({
    url,
    data: params,
    header: { 'Content-Type': 'application/x-www-form-urlencoded' },
    method: 'GET',
    dataType: 'json',
    responseType: 'text',
    success: (res) => {
      resolved(res);
    },
    fail: (res) => {
      rejected(res);
    },
    complete: () => {
      if (showloading) {
        wx.hideLoading();
      }
    },
  });
});

/**
 * 旧post请求
 * @param {string} url 请求地址
 * @param {object} params 请求参数
 */
const cpost = (url, params) => new Promise((resolved, rejected) => {
  if (showloading) {
    wx.showLoading({
      title: '加载中...',
      mask: false,
    });
  }
  wx.request({
    url,
    data: params,
    header: { 'Content-Type': 'application/x-www-form-urlencoded' },
    method: 'POST',
    dataType: 'json',
    responseType: 'text',
    success: (res) => {
      resolved(res);
    },
    fail: (res) => {
      rejected(res);
    },
    complete: () => {
      if (showloading) {
        wx.hideLoading();
      }
    },
  });
});

module.exports = {
  get,
  post,
  cget,
  cpost,
};
