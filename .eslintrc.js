module.exports = {
  root: true, // 意思是到头啦，不要再向上找了
  env: { // 代码将会在哪些环境中运行。每个环境都附带了一组特定的预定义全局变量，如 browser 中有 window，document等，添加后可以直接在代码中使用，而不报错。
    browser: true,
    // node: true,
  },
  globals: {
    wx: true,
    App: true,
    Page: true,
    getApp: true,
    Component: true,
  },
  extends: 'airbnb-base', // 使用airbnb风格
  parserOptions: {
    ecmaVersion: 6,
    parser: 'babel-eslint',
    sourceType: 'module',
  },
  rules: {
    'no-console': 'off', // 不使用console
    'no-debugger': 'off', // 不使用debugger
    'linebreak-style': ['off', 'windows'],
    'no-restricted-syntax': 'off',
    'no-plusplus': 'off',
    'import/named': 'off',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'no-use-before-define': 'off',
    'prefer-promise-reject-errors': 'off',
    'arrow-parens': 'off',
    'max-len': 180,
  },
};
