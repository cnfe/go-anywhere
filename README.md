# const-mp-wx

`const-cli`模板项目，此项目为`微信小程序`模板

``` shell
npm i const-cli -g
const init Project_Name mp-wx
```

本模板集成了：

- @vant/weapp
- eslint

## 一、模板如何使用

### 1.1 通过cli新建项目

``` shell
const init Project_Name mp-wx
```

### 1.2 安装依赖

在项目根目录执行：

``` shell
npm install
```

### 1.3 构建npm

在微信开发者工具，启用npm：

> 详细-本地设置-使用npm模块

然后：

> 微信开发者工具-工具-构建npm


## LICENSE

[MIT](LICENSE)